// ### AUTHEN ###
export const PATH_AUTHEN_LOGIN = "authen/login";
export const PATH_AUTHEN_REGISTER = "authen/register";

export const PATH_QUESTION_TYPE = "question/default";

export const PATH_CATEGORY_TYPE = "category/default";

export const PATH_COURSE = (data) => `course?filter_cate=${data}`
export const PATH_COURSE_CREATE = "course/create";
export const PATH_COURSE_UPDATE = "course/update";
export const PATH_COURSE_DELETE = (id) => `course/delete?id=${id}`;


