import { getStored } from "../store/index";

const _token = getStored("token");

export const URL = "http://139.59.229.66:7007/api/v1/";

export const HEADER_JSON = {
  "Content-Type": "application/json",
  Authorization: `${_token}`
};

export const HEADER_MULTIPART = {
  "Content-Type": "multipart/form-data",
  Accept: "multipart/form-data",
  Authorization: `${_token}`
};

export const ROLE = {
  ADMIN: "Admin",
  USER: "User",
};
