import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import Vuex from "vuex";
import VueRouter from "vue-router";
import StoreData from "./vuex/index";
import { routes } from "./routers/index";
import { initialize } from "./helpers/role_permision";

Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(StoreData);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

initialize(router)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
