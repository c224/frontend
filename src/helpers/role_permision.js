import { getStored } from '../store/index'
export const initialize = (router) => {
    router.beforeEach((to, from, next) => {
        // const { path } = to;
        const { authorize } = to.meta;

        // const role_user = store.getters["auth/roleUser"];
        const role_user = getStored("role");

        const isAllowed = (role) => {
            return authorize.includes(role)
        }

        if (authorize) {
            if (!role_user) {
                // not logged in so redirect to login page with the return url
                next({ path: '/login' });
            }

            // check if route is restricted by role
            if (isAllowed(role_user)) {
                // role not authorised so redirect to home page
                next({ path: '/' });
            }
        }

        next();
    });
}