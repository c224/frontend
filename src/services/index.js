import axios from "axios";
import { URL, HEADER_JSON, HEADER_MULTIPART } from "../configs";

const api_body = axios.create({
    baseURL: URL,
    headers: HEADER_JSON,
    timeout: 10000,
});

const api_form_data = axios.create({
    baseURL: URL,
    headers: HEADER_MULTIPART,
});

export const POST = async (path, obj) => {
    const result = await api_body.post(path, obj).catch((error) => {
        return error;
    });
    return result.data;
};

export const GET = async (path) => {
    const result = await api_body.get(path).catch((error) => {
        return error;
    });
    return result.data;
}

export const DELETE = async (path) => {
    const result = await api_body.delete(path).catch((error) => {
        return error;
    });
    return result.data;
}

export const POST_FORM_DATA = async (path, formdata) => {
    const result = await api_form_data.post(path, formdata).catch((error) => {
        return error;
    });
    return result.data
}
