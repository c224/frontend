
export const SnackBarStore = {
    namespaced: true,
    state: {
        result: {},
    },
    mutations: {
        SET_MESSAGE(state, payload) {
            state.result.text = payload.text;
            state.result.color = payload.color;
            state.result.timeout = payload.timeout;
        },
    },
    actions: {
        showSnack({ commit }, payload) {
            commit("SET_MESSAGE", payload);
        },
    },
};
