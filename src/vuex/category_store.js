import { GET } from "../services/index";
import { PATH_CATEGORY_TYPE } from "../configs/path_api";

export const CategoryStore = {
    namespaced: true,
    state: {
        result: null,
        loading: true,
        error: false,
    },
    mutations: {
        SET_RESULT(state, payload) {
            state.result = payload;
        },
        SET_ERROR(state, payload) {
            state.error = payload;
        },
        SET_LOADING(state, payload) {
            state.loading = payload;
        }
    },
    getters: {
        result(state) {
            return state.result;
        },
        loading(state) {
            return state.loading;
        },
        error(state) {
            return state.error;
        },
    },
    actions: {  
        async listCategoryType({ commit }) {
            const res = await GET(PATH_CATEGORY_TYPE) || null;

            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                const { result } = res;
                commit("SET_RESULT", result);
                commit("SET_LOADING", false);
            }
        },
    },
};
