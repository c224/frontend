import { POST, GET, DELETE } from "../services/index";
import { PATH_COURSE, PATH_COURSE_CREATE, PATH_COURSE_DELETE, PATH_COURSE_UPDATE } from "../configs/path_api";

export const CourseStore = {
    namespaced: true,
    state: {
        result: null,
        loading: true,
        error: false,
    },
    mutations: {
        SET_RESULT(state, payload) {
            state.result = payload;
        },
        SET_ERROR(state, payload) {
            state.error = payload;
        },
        SET_LOADING(state, payload) {
            state.loading = payload;
        }
    },
    getters: {
        result(state) {
            return state.result;
        },
        loading(state) {
            return state.loading;
        },
        error(state) {
            return state.error;
        },
    },
    actions: {
        async create({ commit }, data) {
            const res = await POST(PATH_COURSE_CREATE, data) || null;

            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                commit("SET_RESULT", true);
                commit("SET_LOADING", false);
            }
        },

        async update({ commit }, data) {
            const res = await POST(PATH_COURSE_UPDATE, data) || null;

            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                commit("SET_RESULT", true);
                commit("SET_LOADING", false);
            }
        },

        async listCourse({ commit }, data) {
            const res = await GET(PATH_COURSE(data)) || null;

            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                const { result } = res
                commit("SET_RESULT", result);
                commit("SET_LOADING", false);
            }
        },

        async delete({ commit }, data) {
            const res = await DELETE(PATH_COURSE_DELETE(data)) || null;

            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                commit("SET_RESULT", true);
                commit("SET_LOADING", false);
            }
        },

    },
};
