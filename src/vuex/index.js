import { SnackBarStore } from "./snackbar_store";
import { AuthStore } from "./authen_store";
import { CourseStore } from "./course_store";
import { QuestionStore } from "./question_store";
import { CategoryStore } from "./category_store";
export default {
    modules: {
        snackbar: SnackBarStore,
        auth: AuthStore,
        course: CourseStore,
        question: QuestionStore,
        category: CategoryStore,
    },
};
