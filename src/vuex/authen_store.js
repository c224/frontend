import { getStored, setStored, removeStored } from "../store/index";
import { POST } from "../services/index";
import { PATH_AUTHEN_LOGIN } from "../configs/path_api";

export const AuthStore = {
    namespaced: true,
    state: {
        role_user: getStored("role"),
        result: null,
        loading: true,
        error: false,
    },
    mutations: {
        SET_LOGIN(state, payload) {
            const { role, full_name, token, account_id } = payload;
            state.role_user = role;

            setStored("role", state.role_user);
            setStored("full_name", full_name);
            setStored("token", token);
            setStored("account_id", account_id);
        },
        SET_LOGOUT(state) {
            removeStored("role");
            removeStored("full_name");
            removeStored("token");
            removeStored("account_id");
            state.role_user = null;
        },
        SET_ERROR(state, payload) {
            state.error = payload;
        },
        SET_LOADING(state, payload) {
            state.loading = payload;
        },
    },
    getters: {
        roleUser(state) {
            return state.role_user;
        },
        loading(state) {
            return state.loading;
        },
        error(state) {
            return state.error;
        },
    },
    actions: {
        async login({ commit }, data) {
            const res = await POST(PATH_AUTHEN_LOGIN, data) || null;
            
            if (res === null) {
                commit("SET_ERROR", true);
            } else {
                const { result } = res;
                const { role, full_name, token, account_id } = result;

                const payload = {
                    full_name,
                    role,
                    token,
                    account_id,
                };

                commit("SET_LOGIN", payload);
                commit("SET_LOADING", false);
            }
        },
        // async register({ commit }, data) {
        //   let res = await auth.register(data);
        //   if (res == 400 || res == 500 || res == 404 || res == 409 || res == 401) {
        //     commit("SET_ERROR", true);
        //   } else {
        //     commit("SET_ERROR", false);
        //     commit("SET_RESULT", true);
        //   }
        // },
        logout({ commit }) {
            commit("SET_LOGOUT");
        },
    },
};
