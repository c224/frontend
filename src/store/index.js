export const getStored = (filedName) => {
    return localStorage.getItem(filedName) && localStorage.getItem(filedName) !== "undefined"
        ? localStorage.getItem(filedName)
        : null;
};

export const setStored = (filedName, value) => {
    return localStorage.setItem(filedName, value);
};

export const removeStored = (filedName) => {
    return localStorage.removeItem(filedName);
};

export const clearStored = () => {
    return localStorage.clear();
};
