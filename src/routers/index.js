
import Course from '../views/course/course.vue'
import CreateCourse from '../views/course/create_course.vue'
import Login from '../views/authen/login_form.vue'
import NotFound from '../views/not_found.vue'
// import { ROLE } from "../configs/index";
// const { ADMIN } = ROLE;

export const routes = [
    {
        path: '*',
        name: '404',
        component: NotFound,
    },
    {
        path: "/login",
        name: "login",
        component: Login,
    },
    {
        path: "/",
        name: "course",
        component: Course,
        meta: { authorize: [] },
    },
    {
        path: "/create-course",
        name: "create-course",
        component: CreateCourse,
        meta: { authorize: [] },
    },
]